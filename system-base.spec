Name: system-base
Version: 7.6.5
Release: 1%{dist}
Summary: Initializes the system environment
License: GPLv3 or later
Group: Applications/System
Source: %{name}-%{version}.tar.gz
# Upgrade path with new security driver
Obsoletes: clearos-base
Provides: clearos-base
# Pull in security driver
Requires: system-base-security
# Base packages
Requires: gnupg2
Requires: kernel >= 3.10.0
Requires: man-db
Requires: man
Requires: mlocate
Requires: openssh-clients
Requires: pam
Requires: selinux-policy-targeted
Requires: sudo
Requires: rsyslog
Requires: yum
# Common tools used in install and upgrade scripts for app-* packages
Requires: chkconfig
Requires: coreutils
Requires: findutils
Requires: gawk
Requires: grep
Requires: sed
Requires: shadow-utils
Requires: util-linux
Requires: which
Requires: /usr/bin/logger
Requires: /sbin/pidof

%description
Initializes the system environment.

%prep
%setup -q
%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
mkdir -p -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
mkdir -p -m 755 $RPM_BUILD_ROOT%{_sbindir}
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/clearos
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/clearos/bin
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/clearos/apps
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/clearos/sandbox
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/clearos/themes
mkdir -p -m 755 $RPM_BUILD_ROOT/var/clearos
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/systemd/system.conf.d

install -m 644 system-base-logrotate $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/system
install -m 644 system-base-profile.sh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/system-base.sh
install -m 755 addsudo $RPM_BUILD_ROOT%{_sbindir}/addsudo
install -m 644 clearos-base.conf $RPM_BUILD_ROOT%{_sysconfdir}/systemd/system.conf.d/clearos-base.conf
install -D -m 0644 rsyslogd-system-base.conf %{buildroot}/etc/rsyslog.d/system-base.conf


#------------------------------------------------------------------------------
# I N S T A L L  S C R I P T
#------------------------------------------------------------------------------

%post
logger -p local6.notice -t installer "system-base - installing"

# Syslog customizations
#----------------------

if [ -z "`grep ^local6 /etc/rsyslog.conf`" ]; then
    logger -p local6.notice -t installer "system-base - adding system log file to rsyslog"
    echo "local6.*  /var/log/system" >> /etc/rsyslog.conf
    sed -i -e 's/[[:space:]]*\/var\/log\/messages/;local6.none \/var\/log\/messages/' /etc/rsyslog.conf
fi
/sbin/service rsyslog condrestart >/dev/null 2>&1

# Sudo policies
#--------------

CHECKSUDO=`grep '^Defaults:root !syslog' /etc/sudoers 2>/dev/null`
if [ -z "$CHECKSUDO" ]; then
    logger -p local6.notice -t installer "system-base - adding syslog policy for root"
    echo 'Defaults:root !syslog' >> /etc/sudoers
    chmod 0440 /etc/sudoers
fi

CHECKTTY=`grep '^Defaults.*requiretty' /etc/sudoers 2>/dev/null`
if [ -n "$CHECKTTY" ]; then
    logger -p local6.notice -t installer "system-base - removing requiretty from sudoers"
    sed -i -e 's/^Defaults.*requiretty/# Defaults    requiretty/' /etc/sudoers
    chmod 0440 /etc/sudoers
fi

# slocate/mlocate upgrade
#------------------------

CHECK=`grep '^export' /etc/updatedb.conf 2>/dev/null`
if [ -n "$CHECK" ]; then
    CHECK=`grep '^export' /etc/updatedb.conf.rpmnew 2>/dev/null`
    if ( [ -e "/etc/updatedb.conf.rpmnew" ] && [ -z "$CHECK" ] ); then
        logger -p local6.notice -t installer "system-base - migrating configuration from slocate to mlocate"
        cp -p /etc/updatedb.conf.rpmnew /etc/updatedb.conf
    else
        logger -p local6.notice -t installer "system-base - creating default configuration for mlocate"
        echo "PRUNEFS = \"auto afs iso9660 sfs udf\"" > /etc/updatedb.conf
        echo "PRUNEPATHS = \"/afs /media /net /sfs /tmp /udev /var/spool/cups /var/spool/squid /var/tmp\"" >> /etc/updatedb.conf
    fi
fi

# check to see if systemd is set to notice and rerun if it is present
#------------------------
if [[ "$(egrep -Re '^LogLevel=' /etc/systemd/* | wc -l)" -gt "0" ]]; then
    if [ -f /etc/systemd/system.conf.d/clearos-base.conf ]; then
        if [[ "$(egrep -Re '^LogLevel=' /etc/systemd/system.conf.d/clearos-base.conf)" == "LogLevel=notice" ]]; then
            /usr/bin/systemd-analyze set-log-level notice
        fi
    fi
fi

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer "system-base - uninstalling"
fi

%files
%defattr(-,root,root)
%dir /etc/clearos
%dir /usr/clearos
%dir /usr/clearos/bin
%dir /usr/clearos/apps
%dir /usr/clearos/sandbox
%dir /usr/clearos/themes
%dir /var/clearos
%{_sysconfdir}/logrotate.d/system
%{_sysconfdir}/profile.d/system-base.sh
%{_sbindir}/addsudo
%config(noreplace) /etc/systemd/system.conf.d/clearos-base.conf
/etc/rsyslog.d/system-base.conf

